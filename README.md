# jBootstrap

jBootstrap is a collection of Joomla-related Thor tasks and abstract templates. It makes developing a new Joomla component rather trivial.

Note that jBootstrap is programmed for Joomla 1.5, which is unfortunately the requirement I had when developing the software. I'll accept pull requests into other branches if someone wants to update it for newer versions.

## Pre-requisites

- Ruby >= 1.9.2
- Bundler

## Use

1. Clone the repository.
2. `cd jbootstrap && bundle install`
3. `cd .. && thor install jbootstrap`
4. `cd NEW_EXTENSION_DIRECTORY`
5. `thor jbootstrap:generate:component NAME`

This will install the jbootstrap namespaces into the `thor` command so you can use it anywhere.

## Advanced Use

### Generators

#### Component generator

Run by calling `thor jbootstrap:generate:component NAME`, where `NAME` is the name of your component (i.e. 'Movies').

#### Backend scaffold generator


First clone the repository

There are no examples at present (all of them are proprietary, sorry), but the basic workflow is inheriting from the abstract  instead of the default Joomla controllers and models, etc...

Good luck!
