Gem::Specification.new do |s|
  s.name        = 'jbootstrap'
  s.version     = '0.0.5.1'
  s.date        = '2012-11-12'
  s.summary     = 'Generator and abstract classes for Joomla 1.5 components.'
  s.description = 'Generator and abstract classes for Joomla 1.5 components.'
  s.author      = 'Rob Yurkowski'
  s.email       = 'rob@yurkowski.net'

  s.require_paths = %w[lib]

  s.executables = %w[jbootstrap]
  #s.default_executable = 'jbootstrap'

  s.add_dependency 'thor', '~> 0.16.0'

  s.files       = `git ls-files`.split("\n")
end
