$:.unshift(File.expand_path(File.join(File.dirname(__FILE__), *%w[.. lib])))

begin
  require 'rubygems'
rescue LoadError
end

require 'thor'
require 'thor/group'
require 'thor/runner'
require 'date'

require 'jbootstrap/generators/component'
require 'jbootstrap/generators/frontend'
require 'jbootstrap/generators/backend'
require 'jbootstrap/runner'
