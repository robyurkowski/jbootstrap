module JBootstrap
  module Generators
    class FrontendScaffold < Thor::Group
      include Thor::Actions

      desc "Generates a new frontend scaffold."
      argument :scaffold_name, :type => :string, :desc => "The name of the resource to be created."

      def self.source_root
        File.expand_path(File.join(File.dirname(__FILE__), %w[.. templates frontend]))
      end

      def set_variables
        @scaffold_name = scaffold_name.downcase
        @scaffold_singular_name = options[:singular_name] || @scaffold_name.chop
        @component_name = options[:component_name] || File.basename(Dir.pwd).sub('com_', '')
        @component_singular_name = options[:component_singular_name] || @component_name.chop
      end

      def copy_files
        template "site/models/SC_NAME_SINGULAR.php.tt", "site/models/#{@scaffold_singular_name}.php"
        template "site/views/SC_NAME_SINGULAR/view.html.php.tt", "site/views/#{@scaffold_singular_name}/view.html.php"
        template "site/views/SC_NAME_SINGULAR/tmpl/default.php.tt", "site/views/#{@scaffold_singular_name}/tmpl/default.php"
      end

      def inject_manifest
        inject_into_file "com_#{@component_name.downcase}.xml", :after => %Q{<files folder="site">\n} do
          "\t\t<filename>site/models/#{@scaffold_singular_name}.php</filename>\n" +
          "\t\t<filename>site/views/#{@scaffold_singular_name}/view.html.php</filename>\n" +
          "\t\t<filename>site/views/#{@scaffold_singular_name}/tmpl/default.php</filename>\n"
        end
      end
    end
  end
end
