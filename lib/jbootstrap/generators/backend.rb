module JBootstrap
  module Generators
    class BackendScaffold < Thor::Group
      include Thor::Actions

      desc "Generates a new backend scaffold."
      argument :scaffold_name, :type => :string, :desc => "The name of the resource to be created."

      def self.source_root
        File.expand_path(File.join(File.dirname(__FILE__), %w[.. templates backend]))
      end

      def set_variables
        @scaffold_name = scaffold_name.downcase
        @scaffold_singular_name = options[:singular_name] || @scaffold_name.chop
        @component_name = options[:component_name] || File.basename(Dir.pwd).sub('com_', '').downcase
        @component_singular_name = options[:component_singular_name] || @component_name.chop
      end

      def copy_files
        template "admin/controllers/SC_NAME.php.tt", "admin/controllers/#{@scaffold_name}.php"
        template "admin/models/SC_SINGULAR_NAME.php.tt", "admin/models/#{@scaffold_singular_name}.php"
        template "admin/tables/SC_SINGULAR_NAME.php.tt", "admin/tables/#{@scaffold_singular_name}.php"
        template "admin/views/SC_SINGULAR_NAME/view.html.php.tt", "admin/views/#{@scaffold_singular_name}/view.html.php"
        template "admin/views/SC_SINGULAR_NAME/tmpl/default.php.tt", "admin/views/#{@scaffold_singular_name}/tmpl/default.php"
        template "admin/views/SC_SINGULAR_NAME/tmpl/list.php.tt", "admin/views/#{@scaffold_singular_name}/tmpl/list.php"
      end

      def inject_manifest
        inject_into_file "com_#{@component_name}.xml", :after => %Q{<files folder="admin">\n} do
          "\t\t\t<filename>admin/controllers/#{@scaffold_name}.php<filename>\n" +
          "\t\t\t<filename>admin/models/#{@scaffold_singular_name}.php</filename>\n" +
          "\t\t\t<filename>admin/tables/#{@scaffold_singular_name}.php</filename>\n" +
          "\t\t\t<filename>admin/views/#{@scaffold_singular_name}/view.html.php</filename>\n" +
          "\t\t\t<filename>admin/views/#{@scaffold_singular_name}/tmpl/default.php</filename>\n" +
          "\t\t\t<filename>admin/views/#{@scaffold_singular_name}/tmpl/list.php</filename>\n"
        end
      end

      def update_acceptable_controllers
        gsub_file "admin/admin.#{@component_name}.php", 
                  /\$component_acceptable_controllers = array\((.+?)\);/,
                  "$component_acceptable_controllers = array(\1, #{@scaffold_name})"
      end
    end
  end
end
