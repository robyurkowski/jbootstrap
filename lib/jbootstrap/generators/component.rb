module JBootstrap
  module Generators
    class Component < Thor::Group

      include Thor::Actions
      attr_accessor :name, :description, :date, :singular_name

      desc "Generates a new component from template."
      argument :name, :type => :string, :desc => "The human name of the component"

      def self.source_root
        File.expand_path(File.join(File.dirname(__FILE__), %w[.. templates component]))
      end

      def set_variables
        @name = name
        @singular_name = name.chop
        @description = description
        @date = Date.today.strftime '%B %Y'
      end

      def create_readme
        template 'README.md.tt'
      end

      def create_manifest
        template "com_COM_NAME.xml.tt", "com_#{@name.downcase}.xml"
      end

      def create_backend
        directory "admin/abstracts"
        copy_file "admin/install.php", "admin/install.php"
        copy_file "admin/uninstall.php", "admin/uninstall.php"
        template "admin/admin.COM_NAME.php.tt", "admin/admin.#{@name.downcase}.php"
        template "admin/language/en-GB/en-GB.com_COM_NAME.ini.tt",      "admin/language/en-GB/en-GB.com_#{@name.downcase}.ini"
        template "admin/language/en-GB/en-GB.com_COM_NAME.menu.ini.tt", "admin/language/fr-FR/fr-FR.com_#{@name.downcase}.ini"
        template "admin/language/en-GB/en-GB.com_COM_NAME.menu.ini.tt", "admin/language/en-GB/en-GB.com_#{@name.downcase}.menu.ini"
        template "admin/language/en-GB/en-GB.com_COM_NAME.menu.ini.tt", "admin/language/fr-FR/fr-FR.com_#{@name.downcase}.menu.ini"
      end

      def create_frontend
        directory "site/abstracts"
        template "site/language/en-GB/en-GB.com_COM_NAME.ini.tt", "site/language/en-GB/en-GB.com_#{@name.downcase}.ini"
        template "site/language/fr-FR/fr-FR.com_COM_NAME.ini.tt", "site/language/fr-FR/fr-FR.com_#{@name.downcase}.ini"
        template "site/COM_NAME.php.tt", "site/#{@name.downcase}.php"
        template "site/controller.php.tt"
      end

      def create_helpers
        copy_file "gitignore", ".gitignore"
        copy_file "Guardfile"
        copy_file "Gemfile"
        run "bundle check"
        run "bundle install"
      end

      def init_git
        run "git init ."
        run "git add -A"
        run "git commit -m 'Initial import.'"
      end
    end
  end
end
