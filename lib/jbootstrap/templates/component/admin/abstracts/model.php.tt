<?php
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');

class AbstractModel extends JModel
{
  var $_db;
  var $_tablename;
  var $_table;
  var $_object_name;
  var $_collection_name;

  var $_collection = null;
  var $_total = null;
  var $_pagination = null;
  
  function __construct($object_name, $collection_name = '')
  {
    parent::__construct();

    $this->_object_name = $object_name;
    $this->_collection_name = (($collection_name === '') ? $object_name . 's' : $collection_name);
    $this->_db = $this->getDBO();
    $this->_table =& $this->getTable($this->_object_name);

    $this->setSortingInfo();
  }

  // Gets the collection for the index
  public function getCollection()
  {
    if (empty($this->_collection))
    {
      $query = $this->getIndexQuery() . $this->getIndexOrder();
      $limitstart = $this->getState('limitstart');
      $limit = $this->getState('limit');

      $this->_collection = $this->_getList($query, $limitstart, $limit);
    }

    return $this->_collection;
  }

  // Retrieves the pagination object for the index.
  public function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');

      $total = $this->getTotal();
      $limitstart = $this->getState('limitstart');
      $limit = $this->getState('limit');

      $this->_pagination = new JPagination($total, $limitstart, $limit);
    }

    return $this->_pagination;
  }

  // Returns the total number of records on the index page.
  public function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->getIndexQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  // Retrieves a single item.
  public function getItem($id)
  {
    $query = sprintf("SELECT * FROM %s WHERE %s = %d",
               $this->getTableName(),
               $this->nq('id'),
               (int)$id);

    $this->_db->setQuery($query);
    
    $item = $this->_db->loadObject();
    if (is_object($item)) return $item;
    else JError::raiseError(500, JText::sprintf('COM_<%= @name.upcase %>_OBJECT_NOT_FOUND', $this->_object_name, $id));
  }

  public function getItemByOrdering($position)
  {
    $query = sprintf("SELECT %s FROM %s WHERE %s = %d LIMIT 1",
                     $this->nq('id'),
                     $this->getTableName(),
                     $this->nq('ordering'),
                     (int) $position);

    $this->_db->setQuery($query);
    return $this->_db->loadObject();
  }

  public function getCollectionByOrdering()
  {
    $query = sprintf("SELECT %s FROM %s ORDER BY %s",
                     $this->nq('id'),
                     $this->getTableName(),
                     $this->nq('ordering'));

    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }

  // Gets an empty item which can be used to save.
  public function getNewItem()
  {
    $this->_table->id = 0;
    return $this->_table;
  }

  // Persist the item to the database, performing validations.
  public function store()
  {
    $item = JRequest::get('post');
    $this->beforeStoreCallback($item);

    if (!$this->_table->bind($item)) return $this->failWithDBError();
    if (isset($this->_table->ordering) && !$this->_table->id)
    {
      $where = 'region_id = ' . (int) $row->region_id;
      $this->_table->ordering = $this->_table->getNextOrder($where);
    }

    if (!$this->_table->check()) return $this->failWithTableError();
    if (!$this->_table->store()) return $this->failWithDBError();
    $inserted_id = $this->_table->id;

    if ($this->afterStoreCallback($item, $inserted_id))
    {
      return true;
    }
    else
    {
      $this->delete(array($inserted_id));
      $this->setError($this->afterStoreCallbackFailureMessage());
      return false;
    }
  }

  protected function beforeStoreCallback(&$item)
  {
    $this->_table->reset();
  }

  protected function afterStoreCallback(&$item, $new_id)
  {
    return true;
  }

  protected function afterStoreCallbackFailureMessage()
  {
    return JText::_('COM_<%= @name.upcase %>_AFTER_SAVE_CALLBACK_FAILED');
  }

  // Delete the item from the database.
  public function delete($cids)
  {
    $field = $this->_db->nameQuote('id');
    $query = sprintf("DELETE FROM %s WHERE %s in (%s)",
               $this->getTableName(),
               $field,
               implode(",", $cids));

    $this->_db->setQuery($query);
    if (!$this->_db->query())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    return true;
  }

  // For running Quote a bit shorter.
  public function q($text)
  {
    return $this->_db->Quote($text);
  }

  // For running nameQuote a little bit shorter
  public function nq($text)
  {
    return $this->_db->nameQuote($text);
  }

  // Override this to change the query performed by the index action.
  protected function getIndexQuery()
  {
    return "SELECT * FROM {$this->getTableName()}";
  }

  // Used to specify ordering from headers inside an index.
  protected function getIndexOrder()
  {
    global $mainframe, $option;
    $filter_order = $this->getState('filter_order');
    $filter_order_Dir = $this->getState('filter_order_Dir');

    return ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
  }

  // Override this to change the default sort column.
  protected function getDefaultSortColumn()
  {
    return 'id';
  }

  // This must be overridden to allow sorting by other atributes.
  protected function getSortableAttributes()
  {
    return array('id');
  }

  // Used to get a namequoted version of the model's table.
  protected function getTableName()
  {
    if (!empty($this->_tablename)) return $this->_tablename;

    $this->_tablename = $this->_db->nameQuote('#__<%= @name.downcase %>_' . $this->_collection_name);
    return $this->_tablename;
  }

  // Die, retrieving the error message from the database
  protected function failWithDBError()
  {
    $this->setError($this->_db->getErrorMsg());
    return false;
  }

  // Die, retrieving the error from the table object
  private function failWithTableError()
  {
    $this->setError($this->_table->getError());
    return false;
  }

  // Sets sorting state in model construction
  protected function setSortingInfo()
  {
    global $mainframe, $option;
    $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'));
    $limitstart = $mainframe->getUserStateFromRequest($option . $this->_collection_name . 'limitstart', 'limitstart', 0);
    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);

    $filter_order = $mainframe->getUserStateFromRequest($option . $this->_object_name . 'filter_order', 'filter_order', $this->getDefaultSortColumn(), 'cmd');
    $filter_order_Dir = strtoupper($mainframe->getUserStateFromRequest($option . $this->_object_name . 'filter_order_Dir', 'filter_order_Dir', 'ASC', 'word'));

    // Do some whitelisting 
    if ($filter_order_Dir != 'ASC' && $filter_order_Dir != 'DESC') $filter_order_Dir = 'ASC';
    if (!in_array($filter_order, $this->getSortableAttributes())) $filter_order = $this->getDefaultSortColumn();

    if ($filter_order == 'ordering') $filter_order = 'region_id, ordering';

    $this->setState('filter_order', $filter_order);
    $this->setState('filter_order_Dir', $filter_order_Dir);
  }

}

?>
