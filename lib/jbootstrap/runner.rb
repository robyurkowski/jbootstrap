require 'jbootstrap'

module JBootstrap
  class Runner < ::Thor
    
    desc "component NAME", "generates a new component"
    def component(name)
      JBootstrap::Generators::Component.start([name])
    end

    desc "scaffold SCAFFOLD_NAME", "generates a new scaffold"
    method_options %w[all -a] => false
    method_options %w[frontend -f] => false
    method_options %w[backend -b] => false
    def scaffold(scaffold_name)
      if options[:all]
        JBootstrap::Generators::BackendScaffold.start([scaffold_name])
        JBootstrap::Generators::FrontendScaffold.start([scaffold_name])
      elsif options[:frontend]
        JBootstrap::Generators::FrontendScaffold.start([scaffold_name])
      elsif options[:backend]
        JBootstrap::Generators::BackendScaffold.start([scaffold_name])
      else
        puts "You need to specify which type of scaffold you'd like to create ([-a]ll, [-f]rontend, or [-b]ackend.)"
      end
    end
  end
end

JBootstrap::Runner.start
